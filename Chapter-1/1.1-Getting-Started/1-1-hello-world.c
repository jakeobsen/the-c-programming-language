#include <stdio.h>
/* if stdio have not been included, the compiler WILL produce a warning, saying
     you should include it. Thus it have been included, and will be for all
     future programs.  */

int main()
/* main() WILL produce a warning at compile time, since no type was define.
     For the sake of a "silent" compilation as the book describes, main() has
     been declared an int, and will be for all future programs, unless declared
     as something else.  */
{
    printf("hello, world\n");
}


// Answer:

/*  6: leaving out main() will produce an error, and no compilation:
           error: expected identifier or ‘(’ before ‘{’ token  */

/* 12: leaving out ; will produce an error, and no compilation:
           error: expected ‘;’ before ‘}’ token  */

/* 13: leaving out } will produce an error, and no compilation:
           expected declaration or statement at end of input  */
