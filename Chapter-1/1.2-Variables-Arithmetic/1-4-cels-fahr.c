#include <stdio.h>

/*  print Fahrenheit-Celsius table
       for f = 0, 20, ..., 300  */

int main()
{
    int lower, upper, step;
    float fahr, celsius;

    lower = -20;        // lower limit of temperature table
    upper = 150;      // upper limit
    step  = 10;       // step size

    printf("Censius  Fahr\n");

    celsius = lower;
    while (celsius <= upper) {
        fahr = celsius * (9.0/5.0) + 32.0;
        printf("%3.0f       %3.0f\n", celsius, fahr);
        celsius = celsius + step;
    }
}
