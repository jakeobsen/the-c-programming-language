#include <stdio.h>
// use test.txt for testing: ./a.out < test.txt

int main ()  // count lines in input
{
    int c, nl, tab, blank;

    nl    = 0;
    tab   = 0;
    blank = 0;
    while ((c = getchar()) != EOF)
        if (c == '\n')
            ++nl;
        else if (c == '\t')
            ++tab;
        else if (c == ' ')
            ++blank;
    printf("New lines: %d\nTabs: %d\nBlanks: %d\n", nl, tab, blank);
}
