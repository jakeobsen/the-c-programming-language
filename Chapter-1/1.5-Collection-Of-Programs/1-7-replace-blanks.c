#include <stdio.h>
// use test.txt for testing: ./a.out < test.txt

int main()  // replace multiple blanks
{
    int c, blank;
    blank = 0;

    while ((c = getchar()) != EOF)
    {
        if (c == ' ' && blank == 1)
            ;
        else
            putchar(c);
        
        if (c == ' ')
            blank = 1;
        else
            blank = 0;
    }
}
