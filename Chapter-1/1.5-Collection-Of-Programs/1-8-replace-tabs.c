#include <stdio.h>
/*  Due to the time difference since 1978, this excercise is obsolete
      and the goal cannot be achieved, thus i have modified it with a
      utf-8 arrow instead. */
int main()
{
    int c;
    while ((c = getchar()) != EOF)
    {
        if (c == '\t')
            printf("→");
        else if (c == '\b')
            printf("←");
        else
            putchar(c);
    }
}
