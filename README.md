# the-c-programming-language

*NB: I got my hands ont he ANSI edition, so I decided to abandon this repo.
     I created another repo, to be used with the ANSI edition, in case I
     decide to return here. The repo is available [Here][1]* 

This is my repo, that I use to store code I write while reading The C
Programming Language

Essentially this is the "answers" i come up with while reading through. Some
files may contain an answer writted directly in the file, while some might just
be a copy of the example in the book, with no "answer".

The files are organised by a chapter-excercise structure, example:

    1-1-hello-world.c
    │ │ │
    │ │ └ Defines the name I chose
    │ │
    │ └ Defines the excercise number
    │
    └ Defines the chapter in the book we're at

For other files, I will just name them pagenumber-hello-world.c, theese files
   are just example code files, that is provided by the book, the number
   represents the page number the code is on, you can find the right file by
   looking at the chapter, then open the chapter folder, and then find the file
   with the page number, matching the book page number.

For all files, I will add a comment if i change the code from the book, given
   the book is a 1978 version of non-ANSI C, things have changed. I'm basing my
   code on the 2011 version of C.

For best use of this repo, get the book. This is based off the 1978 book.

I'm in no way liable for any use of this repo.


[1]: https://github.com/jakeobsen/the-c-programming-language-ansi The C Programming Language
